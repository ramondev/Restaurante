package views;

import javax.swing.JOptionPane;

/**
 * @author ramon
 */
public class MenuPrincipal {

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        String op = JOptionPane.showInputDialog(null,
                "RESTAURANTE\n\n"
                + "1 - Pedidos\n"
                + "2 - Produtos\n"
                + "3 - Vendedores\n"
                + "0 - Sair"
        );

        switch (op) {
            case "1":

                break;
            case "2":
                MenuProdutos mp = new MenuProdutos();
                mp.abrir();
                break;
            case "3":

                break;
            case "0":
                JOptionPane.showMessageDialog(null,
                        "Volte sempre!");
                break;
            default:
                JOptionPane.showMessageDialog(null,
                        "Opção é inválida!");
                menu();
        }
    }

}
