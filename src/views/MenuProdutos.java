package views;

import controllers.CtrlProdutos;
import javax.swing.JOptionPane;
import models.Produtos;

/**
 * @author ramon
 */
public class MenuProdutos {

    private CtrlProdutos control = new CtrlProdutos();

    public void abrir() {
        String op = JOptionPane.showInputDialog(
                "MENU PRODUTOS\n\n"
                + "1 - Novo\n"
                + "2 - Listar\n"
                + "0 - Sair\n"
        );

        switch (op) {
            case "1":
                String descricao = JOptionPane.showInputDialog(
                        "Digite o nome do novo produto:"
                );
                double valor = Double.parseDouble(JOptionPane.showInputDialog(
                        "Digite o valor do novo produto:"
                ).replace(",", "."));
                if (control.gravar(descricao, valor)) {
                    JOptionPane.showMessageDialog(null,
                            "Produto inserido com sucesso!");
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Não foi possível inserir um novo produto.");
                }
                abrir();
                break;
            case "2":
                String lista = "LISTA DE PRODUTOS:\n\n";
                for (Produtos p : control.listar()) {
                    lista += p.getDescricao().toUpperCase() + "....."
                            + p.getValor() + "\n";
                }
                JOptionPane.showMessageDialog(null, lista);
                abrir();
                break;
            case "0":
                MenuPrincipal.menu();
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opção inválida!");
                abrir();
                break;
        }
    }
}
