
package database;

import java.util.ArrayList;
import models.Pedidos;
import models.Produtos;
import models.Vendedores;

/**
 * @author ramon
 */
public class Database {
    private static ArrayList<Vendedores> vendedores = new ArrayList();
    private static ArrayList<Produtos> produtos = new ArrayList();
    private ArrayList<Pedidos> pedidos = new ArrayList();
    
    public static boolean insertVendedor(Vendedores v){
        return vendedores.add(v);
    }
    
    public static boolean insertProduto(Produtos p){
        return produtos.add(p);
    }
   
    public static ArrayList<Vendedores> selectAllVendedores(){
        return vendedores;
    }
    
    public static ArrayList<Produtos> selectAllProdutos(){
        return produtos;
    }
    
}