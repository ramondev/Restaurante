package controllers;

import java.util.ArrayList;
import models.Produtos;

/**
 * @author ramon
 */
public class CtrlProdutos {

    public boolean gravar(String descricao, double valor) {
        Produtos p = new Produtos();
        p.setDescricao(descricao);
        p.setValor(valor);
        return p.insert();
    }
    
    public ArrayList<Produtos> listar(){
        return Produtos.selectAll();
    }
}
