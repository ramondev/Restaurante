
package models;

import database.Database;
import java.util.ArrayList;

/**
 * @author ramon
 */
public class Produtos {
    
    private int id;
    private String descricao;
    private double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public boolean insert(){
        return Database.insertProduto(this);
    }
    
    public static ArrayList<Produtos> selectAll(){
        return Database.selectAllProdutos();
    }
    
}