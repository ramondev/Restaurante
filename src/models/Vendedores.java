
package models;

import database.Database;
import java.util.ArrayList;

/**
 * @author ramon
 */
public class Vendedores {
    private int id;
    private String nome, matricula, senha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public boolean insert(){
        return Database.insertVendedor(this);
    }
    
    public static ArrayList<Vendedores> selecAll(){
        return Database.selectAllVendedores();
    }
    
}